"""
Fabric file
"""
import os
from fabric.api import lcd, local


def get_project_root():
    """Returns the local project root"""
    project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    return project_root

def test_python():
    """Runs the python tests"""
    local_root_folder = os.path.join(get_project_root(), "root")
    with lcd(local_root_folder): # pylint: disable=E1129
        local("tox")

def test_js():
    """Runs the javascript tests"""
    local_root_folder = os.path.join(get_project_root(), "root")
    with lcd(local_root_folder): # pylint: disable=E1129
        local("npm test -- --single-run")

def test():
    """Runs all the tests"""
    test_python()
    test_js()

def lint_python():
    """Lints the python code"""
    local_root_folder = os.path.join(get_project_root(), "root")
    with lcd(local_root_folder): # pylint: disable=E1129
        local("pylint --rcfile=../.pylintrc cibi")

def lint_js():
    """Lints the javascript code"""
    local_root_folder = os.path.join(get_project_root(), "root")
    with lcd(local_root_folder): # pylint: disable=E1129
        local("npm run eslint -- --format=table")

def lint():
    """Lints all the code"""
    lint_python()
    lint_js()

def push(branch="master"):
    """
    Pushes to master
    :param branch: str - branch to which we push, defaults master
    """
    test()
    lint()
    local("git push origin {branch}".format(branch=branch))
