module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/essential",
    ],
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module",
    },
    "globals": {
        "describe": true,
        "it": true,
        "expect": true,
        "beforeEach": true,
        "afterEach": true,
    },
    "rules": {
        "indent": "off",
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
