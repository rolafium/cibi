import CBObject from "../../js/app/CBObject";

describe("CBObject", () => {
    const config = { data: "data" };

    describe("#constructor()", () => {

        it("Sets the expected attributes", () => {
            const cbo1 = new CBObject();
            const cbo2 = new CBObject({});
            const cbo3 = new CBObject(config);

            expect(cbo1.config).to.be.deep.equal({});
            expect(cbo2.config).to.be.deep.equal({});
            expect(cbo3.config).to.be.deep.equal(config);

            expect(cbo1._meta).to.be.deep.equal({});
            expect(cbo2._meta).to.be.deep.equal({});
            expect(cbo3._meta).to.be.deep.equal({});
        });

    });

    describe("#_etendConfig", () => {

        it("Returns the config", () => {
            const cbo = new CBObject();
            expect(cbo._extendConfig()).to.be.deep.equal({});
            expect(cbo._extendConfig({})).to.be.deep.equal({});
            expect(cbo._extendConfig(config)).to.be.deep.equal(config);
        });

    });

});
