import { router } from "../../js/app/routes/routes";
import { store } from "../../js/app/store/store";
import { createApp } from "../../js/app/app";

describe("App.js", () => {

    describe("createApp", () => {

        it("Creates an app", () => {
            const app = createApp({ store, router });
            expect(app.constructor.name).to.be.equal("Vue");
        });

    });

});
