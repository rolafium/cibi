import StoreState from "../../../js/app/store/StoreState";
import StoreMutations from "../../../js/app/store/StoreMutations";

describe("StoreMutations", () => {

    const mutations = new StoreMutations();

    describe("#updateUser()", () => {

        it("Updates the user state with the provided object", () => {
            const state = StoreState.defaultState;
            const obj = { id: 999, email: "EMAIL", moreData: "MORE_DATA" };

            mutations.updateUser(state, obj);
            expect(state.user.login).to.be.false;
            expect(state.user.data.id).to.be.deep.equal(obj.id);
            expect(state.user.data.email).to.be.deep.equal(obj.email);
            expect(state.user.data.moreData).to.be.undefined;
        });

    });

    describe("#login()", () => {

        it("Logs in a user", () => {
            const state = StoreState.defaultState;
            const obj = { id: 999, email: "EMAIL", moreData: "MORE_DATA" };

            mutations.login(state, obj);
            expect(state.user.login).to.be.true;
            expect(state.user.data.id).to.be.deep.equal(obj.id);
            expect(state.user.data.email).to.be.deep.equal(obj.email);
            expect(state.user.data.moreData).to.be.undefined;
        });
    });

    describe("#login()", () => {

        it("Logs in a user", () => {
            const state = StoreState.defaultState;
            const obj = { id: 999, email: "EMAIL", moreData: "MORE_DATA" };
            state.user.login = true;

            mutations.logout(state, obj);
            expect(state.user.login).to.be.false;
            expect(state.user.data.id).to.be.deep.equal(obj.id);
            expect(state.user.data.email).to.be.deep.equal(obj.email);
            expect(state.user.data.moreData).to.be.undefined;
        });
    });

});
