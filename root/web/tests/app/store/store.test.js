import { store } from "../../../js/app/store/store";

describe("Store", () => {

    describe("state", () => {

        it("Has the user state", () => {
            expect(store.state.user.login).to.not.be.undefined;
            expect(store.state.user.data).to.not.be.undefined;
        });

    });

    describe("mutations", () => {

        it("Has all the expected mutations", () => {
            expect(store.mutations.updateUser).to.not.be.undefined;
            expect(store.mutations.login).to.not.be.undefined;
            expect(store.mutations.logout).to.not.be.undefined;
        });

    });
});
