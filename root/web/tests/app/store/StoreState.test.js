import StoreState from "../../../js/app/store/StoreState";


describe("StoreState", () => {

    describe("#defaultState()", () => {

        it("Generates a complete object", () => {
            const state = StoreState.defaultState;
            const expected = {
                user: { ...StoreState.DEFAULT_USER_STATE },
                recipes: { ...StoreState.DEFAULT_RECIPES_STATE },
            };

            expect(state).to.be.deep.equal(expected);
        });

        it("Each generated default state is independent", () => {
            const state1 = StoreState.defaultState;
            const state2 = StoreState.defaultState;
            expect(state1).to.be.deep.equal(state2);

            state1.abc = "abc";
            expect(state1).to.not.be.deep.equal(state2);
        });
    });

    describe("#constructor()", () => {

    });
});
