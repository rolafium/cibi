import sinon from "sinon";
import * as utils from "../../../js/app/store/utils";

describe("Mutation utils", () => {
    const sandbox = sinon.createSandbox();

    afterEach(() => sandbox.restore());

    describe("#logMutation()", () => {

        it("Console logs the mutation", () => {
            const args = {
                name: "a",
                state: "b",
                obj: "c",
            };

            const logStub = sandbox.stub(global.console, "log");
            utils.logMutation(args.name, args.state, args.obj);
            expect(logStub.calledOnce).to.be.true;
            expect(logStub.args[0][0]).to.be.equal("Vuex.mutation:");
            expect(logStub.args[0][1]).to.be.deep.equal(args);
        });
    });
});
