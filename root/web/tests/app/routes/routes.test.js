import * as routes from "../../../js/app/routes/routes";


describe("createRoute", () => {
    it("It's not undefined", () => {
        expect(routes.createRoute).to.not.be.undefined;
    });

});

describe("scrollBehavior", () => {

    it("It sets the y position to 0", () => {
        const scroll = routes.scrollBehavior;
        const expected = { y: 0 };
        expect(scroll()).to.be.deep.equal(expected);
    });

});
