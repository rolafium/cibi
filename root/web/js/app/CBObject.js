/**
 * Cibi object
 * General signature class from which cibi classes inherit
 */
class CBObject {

    /**
     * @constructor
     * @param {Object} config
     */
    constructor(config = {}) {
        this.config = this._extendConfig(config);
        this._meta = {};
    }

    /**
     * Extends the `config` object
     * before setting it as a property in the constructor
     * @private
     * @param {Object} config
     * @returns {Object}
     */
    _extendConfig(config = {}) {
        return config;
    }

}

export default CBObject;
