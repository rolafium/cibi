import AbstractModel from "./AbstractModel";


class RecipeIngredient extends AbstractModel {

    /** @inheritdoc */
    get fields() {
        return {
            ...super.fields,
            ingredient: null,
            quantity: 1,
        };
    }

    /** @inheritdoc */
    serialize() {
        return {
            id: this.id,
            ingredient: this.ingredient.serialize(),
            quantity: this.quantity,
        };
    }

    static fromIngredient(ingredient, quantity = 1) {
        return new this({
            fields: {
                ingredient,
                quantity,
            },
        });
    }
}

export default RecipeIngredient;
