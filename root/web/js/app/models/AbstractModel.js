import CBObject from "../CBObject";

/**
 * Cibi abstract model
 */
class AbstractModel extends CBObject {

    /**
     * Creates an model object
     * @param {Object} config 
     */
    constructor(config = {}) {
        super(config);
        this.keys = [];
        this._setFields(config.fields);
    }

    /**
     * Gets the defined fields and their defaults
     * @returns {Object}
     */
    get fields() {
        return {
            id: 0,
        };
    }

    /**
     * Sets the fields for the model
     * by joining the provided values and the defaults
     * @param {Object} fields
     * @returns {Object}
     */
    _setFields(fields) {
        const fieldMap = {
            ...this.fields,
            ...fields,
        };

        this.keys = Object.keys(fieldMap);

        this.keys.forEach((key) => {
            this[key] = fieldMap[key];
        });

        return fieldMap;
    }

    serialize() {
        return {};
    }
}

export default AbstractModel;
