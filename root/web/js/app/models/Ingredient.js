import AbstractModel from "./AbstractModel";


class Ingredient extends AbstractModel {

    /** @inheritdoc */
    get fields() {
        return {
            ...super.fields,
            name: "",
            description: "",
            tags: [],
        };
    }

    /** @inheritdoc */
    serialize() {
        return {
            id: this.id,
            name: this.name,
            description: this.description,
            tags: [],
        };
    }
}

export default Ingredient;
