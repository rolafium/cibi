import AbstractModel from "./AbstractModel";


class Recipe extends AbstractModel {

    /** @inheritdoc */
    get fields() {
        return {
            ...super.fields,
            name: "",
            description: "",
            recipeIngredients: [],
            steps: [],
            servings: 1,
        };
    }

    /** @inheritdoc */
    serialize() {
        return {
            id: this.id,
            name: this.name,
            description: this.description,
            recipe_ingredients: this.recipeIngredients.map((ri) => ri.serialize()),
            steps: [],
            number_of_persons: this.servings,
        };
    }
}

export default Recipe;
