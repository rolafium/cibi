import StoreState from "../StoreState";

/**
 * Ingredient mutations
 * @var {Object}
 */
export const ingredientMutations = {

    setIngredients(state, obj) {
        const ingredients = {};
        obj.forEach((ingredient) => ingredients[ingredient.id] = ingredient);
        state.ingredients.map = ingredients;
        state.ingredients.fetched = true;
    },

    pushIngredients(state, obj) {
        obj.forEach((ingredient) => state.ingredients.map[ingredient.id] = ingredient);
    },

    removeIngredients(state) {
        state.recipes = StoreState.DEFAULT_INGREDIENTS_STATE;
        state.ingredients.fetched = false;
    },

};
