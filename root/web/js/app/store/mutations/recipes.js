import StoreState from "../StoreState";

/**
 * Recipe mutations
 * @var {Object}
 */
export const recipeMutations = {

    setRecipes(state, obj) {
        state.recipes.list = obj;
        state.recipes.fetched = true;
    },

    removeRecipes(state) {
        state.recipes = StoreState.DEFAULT_RECIPES_STATE;
        state.recipes.fetched = false;
    },

};
