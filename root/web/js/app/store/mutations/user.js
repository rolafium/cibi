import StoreState from "../StoreState";

/**
 * User mutations
 * @var {Object}
 */
export const userMutations = {

    /**
     * Updates the user data
     * @param {Object} state
     * @param {Object} obj
     */
    updateUser(state, obj) {
        state.user.data.id = obj.id;
        state.user.data.email = obj.email;
        return state;
    },

    /**
     * Logins a user
     * @param {Object} state
     * @param {Object} obj
     */
    login(state, obj) {
        state.user.login = true;
        return userMutations.updateUser(state, obj);
    },

    /**
     * Logouts a user
     * @param {Object} state
     * @param {Object} obj
     */
    logout(state) {
        state.user.login = false;
        return userMutations.updateUser(state, StoreState.DEFAULT_USER_STATE.data);
    },
};
