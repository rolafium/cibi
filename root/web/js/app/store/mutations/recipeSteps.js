import StoreState from "../StoreState";

/**
 * Recipe step mutations
 * @var {Object}
 */
export const recipeStepMutations = {

    setRecipeSteps(state, obj) {
        state.recipes.list = obj;
    },

    removeRecipeSteps(state) {
        state.recipes = StoreState.DEFAULT_RECIPE_STEPS_STATE;
    },

};
