
/**
 * Class wrapper over store mutations
 */
class StoreMutations {

    registerFunction(name, mutation) {
        this[name] = mutation;
    }

    registerObject(mutations) {
        Object.keys(mutations).forEach((key) => {
            this.registerFunction(key, mutations[key]);
        });
    }
}

export default StoreMutations;
