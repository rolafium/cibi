class ModalState {

    constructor() {
        this.reset();
    }

    reset() {
        this.show = false;
        this.state = {};
        this.caller = null;
        this.promise = null;
        this.resolve = null;
        this.reject = null;
    }

    open(caller, state) {
        this.setState(caller, state);
        this.setPromise();
        this.show = true;
        return this.promise;
    }

    setState(caller, state) {
        this.caller = caller;
        this.state = state;
    }

    setPromise() {
        this.promise = new Promise((resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
        });
        return this.promise;
    }

    close(success) {
        if (success) {
            this.resolve(this.state);
        } else {
            this.reject(this.state);
        }
        this.reset();
    }
}

export default ModalState;
