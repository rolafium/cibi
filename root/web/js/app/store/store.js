import StoreState from "./StoreState";
import StoreMutations from "./StoreMutations";
import { userMutations } from "./mutations/user";
import { recipeMutations } from "./mutations/recipes";
import { recipeActions } from "./actions/recipes";
import { ingredientActions } from "./actions/ingredients";
import { ingredientMutations } from "./mutations/ingredients";
import { modalActions } from "./actions/modals";

const state = new StoreState();
const mutations = new StoreMutations();
const actions = {
    ...recipeActions,
    ...ingredientActions,
    ...modalActions,
};

mutations.registerObject(userMutations);
mutations.registerObject(recipeMutations);
mutations.registerObject(ingredientMutations);

export const store = { state, mutations, actions };
