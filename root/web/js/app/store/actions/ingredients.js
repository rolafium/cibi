import DjangoRequest from "../../../backend/DjangoRequest.js";
import { endpoints } from "../../../app/routes/endpoints";

const fetchResource = (url) => {
    return DjangoRequest.send({ url });
};

export const ingredientActions = {
    fetchIngredientsByRecipe(context, recipeId) {
        const url = endpoints.ingredients.byRecipe + "?recipe=" + recipeId;
        fetchResource(url)
        .then((response) => {
            context.commit("pushIngredients", response.json);
        });
    },
    createIngredient(_context, ingredient) {
        const url = endpoints.ingredients.list;
        return DjangoRequest.send({
            url,
            method: "POST",
            data: ingredient,
        });
    }
};
