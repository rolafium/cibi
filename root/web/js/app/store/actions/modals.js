
export const modalActions = {
    openModal(context, modal) {
        const instance = context.state.modals[modal.name];
        return instance.open(modal.caller, modal.state);
    },
    closeModal(context, modal) {
        const instance = context.state.modals[modal.name];
        return instance.close(modal.success);
    },
};
