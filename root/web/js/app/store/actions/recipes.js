import DjangoRequest from "../../../backend/DjangoRequest.js";
import { endpoints } from "../../../app/routes/endpoints";

const fetchRecipes = () => {
    return DjangoRequest.send({
        url: endpoints.recipes.list,
    });
};

export const recipeActions = {
    refreshRecipes(context) {
        return fetchRecipes()
        .then((response) => {
            context.commit("setRecipes", response.json);
        });
    },
    createRecipe(context, recipe) {
        const url = endpoints.recipes.list;
        return DjangoRequest.send({
            url,
            method: "POST",
            data: recipe,
        })
        .then((response) => {
            return recipeActions.refreshRecipes(context);
        });
    }
};
