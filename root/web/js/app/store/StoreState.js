import ModalState from "./ModalState";

/**
 * Wrapper over the store state
 */
class StoreState {

    /**
     * @constructor
     */
    constructor() {
        const state = this.constructor.defaultState;
        Object.keys(state).forEach((key) => {
            this[key] = state[key];
        });
    }

    /**
     * Gets the default state
     * @static
     */
    static get defaultState() {
        return {
            user: { ...this.DEFAULT_USER_STATE },
            recipes: { ...this.DEFAULT_RECIPES_STATE },
            ingredients: { ...this.DEFAULT_INGREDIENTS_STATE },
            recipeSteps: { ...this.DEFAULT_RECIPE_STEPS_STATE },
            modals: { ...this.DEFAULT_MODALS_STATE },
        };
    }
}

/**
 * Default user state
 * @var {Object}
 */
StoreState.DEFAULT_USER_STATE = window.user ? window.user : {
    login: false,
    data: {
        id: 0,
        email: "",
    },
};

/**
 * Default recipes state
 * @var {Object}
 */
StoreState.DEFAULT_RECIPES_STATE = {
    list: [],
    fetched: false,
};

StoreState.DEFAULT_INGREDIENTS_STATE = {
    map: {},
    fetched: false,
};

StoreState.DEFAULT_RECIPE_STEPS_STATE = {
    map: {},
    fetched: false,
};

StoreState.DEFAULT_MODALS_STATE = {
    ingredientModal: new ModalState(),
};


export default StoreState;
