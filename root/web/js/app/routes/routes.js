
/**
 * Lazy loads a component
 * @param {string} component - Name of the component
 * @returns {VueComponent}
 */
export const load = (component) => require(`../../view/pages/${component}.vue`).default;

/**
 * Creates a route
 * @param {string} path
 * @param {VueComponent} component
 * @param {string} routeName
 * @returns {Object}
 */
export const createRoute = (path, component, routeName) => {
    return {
        path: path,
        component: load(component),
        name: routeName,
    };
};

/**
 * List of the routes
 * @var {Array}
 */
export const routes = [
    createRoute("/", "Landing", "landing"),
    createRoute("/recipes", "Recipes/RecipesList", "recipes-list"),
    createRoute("/recipes/:recipe", "Recipes/RecipesDetail", "recipes-detail"),
];

/**
 * Defines the router scroll behaviour
 * @returns {Object}
 */
export const scrollBehavior = () => ({ y: 0 });

/**
 * Frontend app router
 * @var {Object}
 */
export const router = {
    mode: "history",
    scrollBehavior,
    routes,
};
