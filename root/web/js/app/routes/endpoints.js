

export const endpoints = {
    user: {
        list: "/api/user/register/",
        login: "/api/user/login/",
        logout: "/api/user/logout/",
    },
    recipes: {
        list: "/api/recipes/",
    },
    ingredients: {
        list: "/api/ingredients/",
        byRecipe: "/api/ingredients/by-recipe/"
    },
    recipeSteps: {
        list: "/api/recipe-steps/",
    },
};
