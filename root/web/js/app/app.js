import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import { ModalPlugin } from "vueboox";
import CibiApp from "../view/CibiApp.vue";
import { endpoints } from "./routes/endpoints";
import RecipeModal from "../view/organisms/modals/RecipeModal.vue";
import IngredientModal from "../view/organisms/modals/IngredientModal.vue";

Vue.config.productionTip = false;
// Vue.config.devtools = false;

export const createApp = (config) => {
    const context = {
        endpoints,
    };

    Vue.use(Vuex);
    Vue.use(VueRouter);
    Vue.use(ModalPlugin);
    Vue.prototype.context = context;

    const store = new Vuex.Store(config.store);
    const router = new VueRouter(config.router);

    const app = new Vue({
        render: (h) => h(CibiApp),
        store: store,
        router: router,
    });

    registerModals(app);

    return app;
};

export const registerModals = (app) => {
    app.$modals.register("recipe-modal", RecipeModal);
    app.$modals.register("ingredient-modal", IngredientModal);
};
