import Cookies from "js-cookie";
import Request from "./Request";

/**
 * Django request fetcher
 * Extends the Fetcher class to setup a django request
 */
class DjangoRequest extends Request {

    /** @inheritdoc */
    _extendConfig(config = {}) {
        const extendedConfig = super._extendConfig(config);
        extendedConfig.headers.push(this.csrfHeader);
        return extendedConfig;
    }

    /**
     * Returns the X-CSRFToken required by Django
     * @returns {Object}
     */
    get csrfHeader() {
        return {
            name: "X-CSRFToken",
            content: Cookies.get("csrftoken"),
        };
    }

}


export default DjangoRequest;
