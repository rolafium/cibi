import AjaxObject from "./AjaxObject";
import JsonXHR from "./JsonXHR";


/**
 * Ajax request constructor
 */
class Fetcher extends AjaxObject {

    /** @inheritdoc */
    _extendConfig(config = {}) {
        const extendedConfig = {
            headers: [],
            method: "GET",
            async: true,
            data: null,
            preset: this.constructor.preset.JSON,
            ...super._extendConfig(config),
        };

        if (extendedConfig.preset.toUpperCase() === this.constructor.preset.JSON) {
            extendedConfig.headers.push(this.constructor.headers.CONTENT_TYPE_JSON);
        }

        return extendedConfig;
    }

    /**
     * Prepares the data for the request
     * @private
     * @param {*} data
     */
    _prepareData(data) {
        if (this.config.preset === this.constructor.preset.JSON) {
            return JSON.stringify(data);
        } else {
            return data;
        }
    }

    /**
     * Creates a personal object promise
     * @private
     * @returns {Promise}
     */
    _createPromise() {
        return new Promise((resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
        });
    }

    /**
     * Opens the XHR object
     * @private
     * @param {XMLHttpRequest} xhr
     * @returns {XMLHttpRequest}
     */
    _open(xhr) {
        xhr.open(this.config.method, this.config.url, this.config.async);
        return xhr;
    }

    /**
     * Sets the onload listener to the xhr
     * @private
     * @param {XMLHttpRequest} xhr
     * @returns {XMLHttpRequest}
     */
    _setOnload(xhr) {
        this.promise = this._createPromise();

        xhr.addEventListener("load", (e) => {
            xhr.event = e;

            if (xhr.status >= 200 && xhr.status < 300) {
                this.resolve(xhr);
            } else {
                this.reject(xhr);
            }
        });

        return xhr;
    }

    /**
     * Sets the request headers
     * @private
     * @param {XMLHttpRequest} xhr
     * @param {Array} headers
     * @returns {XMLHttpRequest}
     */
    _setHeaders(xhr, headers = []) {
        headers.forEach((header) => this.setHeader(xhr, header));
        return xhr;
    }

    /**
     * Creates and sends the request
     * @private
     * @returns {Promise}
     */
    _send() {
        const xhr = this.getXhr();
        this._open(xhr);
        this._setHeaders(xhr, this.config.headers);
        this._setOnload(xhr);

        if (this.config.data) {
            const requestData = this._prepareData(this.config.data);
            xhr.send(requestData);
        } else {
            xhr.send();
        }

        return this.promise;
    }

    /**
     * Creates and sends the request
     * @param {Object} config
     * @returns {Promise}
     */
    static send(config) {
        const fetcher = new this(config);
        return fetcher._send();
    }
}

/** @inheritdoc */
Fetcher.XML_HTTP_REQUEST_CLASS = JsonXHR;

/**
 * Request presets
 * @var {Object}
 */
Fetcher.preset = {
    JSON: "PRESET_JSON",
};

export default Fetcher;
