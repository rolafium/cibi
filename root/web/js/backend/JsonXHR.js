
/**
 * Extends XMLHttpRequest to support a json field
 */
class JsonXHR extends XMLHttpRequest {

    /** @inheritdoc */
    constructor() {
        super();
        this.json = {};
    }

    /** @inheritdoc */
    addEventListener(eventType, listener, options = {}) {
        let actualListener = listener;

        if (eventType === "load") {
            actualListener = (e) => {
                this.json = JSON.parse(this.responseText);
                listener(e);
            };
        }

        super.addEventListener(eventType, actualListener, options);
    }
}

export default JsonXHR;
