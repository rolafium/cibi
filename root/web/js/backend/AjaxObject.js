import CBObject from "../app/CBObject";

/**
 * Generic ajax object configuration
 * All ajax classes should inherit from AjaxObject
 */
class AjaxObject extends CBObject {

    /**
     * Returns a XMLHttpRequest-like instance
     * @returns {XMLHttpRequest}
     */
    getXhr() {
        const xhr = new this.constructor.XML_HTTP_REQUEST_CLASS();
        xhr._requester = this;
        return xhr;
    }

    /**
     * Sets an header to the XHR object
     * @param {XMLHttpRequest} xhr
     * @param {Object} header
     * @param {string} header.name
     * @param {string} header.content
     */
    setHeader(xhr, header) {
        xhr.setRequestHeader(header.name, header.content);
        return xhr;
    }
}

/**
 * XML Http Request class to be used
 * @var {class}
 */
AjaxObject.XML_HTTP_REQUEST_CLASS = Object;

/**
 * Content types
 * @var {Object}
 */
AjaxObject.contentTypes = {
    JSON: "application/json",
};

/**
 * Default headers
 * @var {Object}
 */
AjaxObject.headers = {
    CONTENT_TYPE_JSON: {
        name: "Content-Type",
        content: AjaxObject.contentTypes.JSON,
    },
};


export default AjaxObject;
