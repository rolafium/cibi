import { router } from "./app/routes/routes.js";
import { store } from "./app/store/store.js";
import { createApp } from "./app/app.js";

const APP_ROOT_ID = "cibi-root";
const APP_ROOT_TAG = "cibi-app";

// Create the root element if it is not defined in the HTML
if (!document.getElementById(APP_ROOT_ID)) {
    const cibiRoot = document.createElement(APP_ROOT_TAG);
    cibiRoot.id = APP_ROOT_ID;
    document.body.appendChild(cibiRoot);
}

const app = createApp({ router, store });
app.$mount(`#${APP_ROOT_ID}`);
window.app = app;
