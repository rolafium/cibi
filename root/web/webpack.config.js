const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const nodeExternals = require("webpack-node-externals");


const mode = process.env.NODE_ENV || "development";

const vueLoaderPlugin = new VueLoaderPlugin();
const miniCssPlugin = new MiniCssExtractPlugin({
    filename: "css/cibi.css",
    chunkFilename: "[id].css"
});

const webpackConfig = {
    devtool : "source-map",
    entry : [
        __dirname + "/js/main.js",
        __dirname + "/scss/main.scss",
    ],
    mode: mode,
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: "vue-loader",
                include: [__dirname + "/js", __dirname + "/tests"],
            },
            {
                test: /\.js$/,
                use: {
                    loader: "istanbul-instrumenter-loader",
                    options: { esModules: true },
                },
                include: __dirname + "/js",
            },
            {
                test: /\.s?css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader",
                ]
            }
        ]
    },
    output : {
        path: __dirname + "/build",
        filename: "js/cibi.js",
    },
    plugins: [
        vueLoaderPlugin,
        miniCssPlugin,
    ],
};

module.exports = webpackConfig;

// test specific setups
if (process.env.NODE_ENV === "development") {
    module.exports.devtool = "source-map";
    module.exports.target = "node";
    module.exports.externals = [nodeExternals()];
  }
