import os
from cibiconf.settings.Base import BaseSettings


class UnitTestsSettings(BaseSettings):

    SECRET_KEY = 'UNIT_TESTING'
    DEBUG = True
    ALLOWED_HOSTS = ["*"]

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BaseSettings.PROJECT_DIR, 'db.test.sqlite3'),
        }
    }


UnitTestsSettings.export_vars()
