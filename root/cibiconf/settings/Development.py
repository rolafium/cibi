import os
from cibiconf.settings.Base import BaseSettings


class DevelopmentSettings(BaseSettings):

    SECRET_KEY = 'DEVELOPMENT'
    DEBUG = True
    ALLOWED_HOSTS = ["*"]

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BaseSettings.PROJECT_DIR, 'db.sqlite3'),
        }
    }


DevelopmentSettings.export_vars()
