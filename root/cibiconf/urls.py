"""
Root url conf
"""
from django.contrib import admin
from django.conf.urls import include
from django.urls import path

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("django_request_cop.urls")),
    path("", include("cibi.urls")),
]
