"""
Admin module for cibi
"""
from django.contrib import admin
from cibi import models


admin.site.register(models.CibiTag)
admin.site.register(models.Ingredient)
admin.site.register(models.Recipe)
admin.site.register(models.RecipeStep)
admin.site.register(models.RecipeFeedback)
admin.site.register(models.RecipeIngredient)
admin.site.register(models.Day)
admin.site.register(models.Meal)
