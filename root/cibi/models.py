"""
Cibi models
"""
import datetime
from django.db import models
from django.utils import timezone

class CibiModel(models.Model):
    """Abstract cibi model"""
    class Meta:
        """Meta class"""
        abstract = True

class CibiTrackedModel(CibiModel):
    """Abstract tracked cibi model"""
    class Meta:
        """Meta class"""
        abstract = True

    created_at = models.DateTimeField(default=timezone.now, help_text="Created at")
    updated_at = models.DateTimeField(default=timezone.now, help_text="Updated at")

    def save(self, *args, **kwargs): #pylint: disable=W0221
        """Saves and set the updated_at field to now"""
        self.updated_at = timezone.now()
        super().save(*args, **kwargs)


class CibiTag(CibiTrackedModel):
    """
    Cibi tag model
    Used to specify metadata of ingredients and recipes
    i.e. "Vegetable", "Spicy", "Expensive", etc
    """
    name = models.CharField(max_length=128, help_text="Name of the tag")

    def __str__(self):
        """String representation of the model"""
        return self.name


class Ingredient(CibiTrackedModel):
    """
    Ingredient model
    The very minimum component of a recipe
    """
    name = models.CharField(max_length=128, help_text="Name of the ingredient")
    description = models.TextField(default="", blank=True, help_text="Description of the ingredient")
    tags = models.ManyToManyField("cibi.CibiTag", help_text="Tags", blank=True)

    def __str__(self):
        """String representation of the model"""
        return self.name

class RecipeIngredient(CibiTrackedModel):
    """
    Ingredient for a recipe
    The quantified ingredient of a recipe
    """
    ingredient = models.ForeignKey("cibi.Ingredient", on_delete=models.CASCADE, help_text="Ingredient")
    quantity = models.FloatField(default=1, help_text="Quantity")
    recipe = models.ForeignKey("cibi.Recipe", on_delete=models.CASCADE, help_text="Recipe")

    def __str__(self):
        """String representation of the model"""
        return "{recipe} - {ingredient} ({quantity})".format(
            recipe=self.recipe.name,
            ingredient=self.ingredient.name,
            quantity=self.quantity,
        )

class Recipe(CibiTrackedModel):
    """
    Recipe model
    The actual things that we want to cook
    """
    name = models.CharField(max_length=128, help_text="Name of the recipe")
    description = models.TextField(default="", blank=True, help_text="Description of the recipe")
    tags = models.ManyToManyField("cibi.CibiTag", help_text="Tags", blank=True)
    number_of_persons = models.FloatField(default=1, help_text="Number of persons")

    @property
    def ingredients(self):
        return self.recipeingredient_set.all()

    @property
    def steps(self):
        return self.recipestep_set.all()

    def __str__(self):
        """String representation of the model"""
        return self.name


class RecipeStep(CibiTrackedModel):
    """
    Recipe step model
    Used to describe each step of the recipe
    """
    recipe = models.ForeignKey("cibi.Recipe", on_delete=models.CASCADE, help_text="Recipe")
    content = models.TextField(help_text="Content of the step")
    optional = models.BooleanField(default=False, help_text="Whether the step is optional")

    def __str__(self):
        """String representation of the model"""
        return "{recipe}'s step".format(recipe=self.recipe.name)


class RecipeFeedback(CibiTrackedModel):
    """
    Recipe feedback model
    Feedback about a recipe
    """
    recipe = models.ForeignKey("cibi.Recipe", on_delete=models.CASCADE, help_text="Recipe")
    author = models.ForeignKey("auth.User", on_delete=models.CASCADE, help_text="Author")
    rating = models.IntegerField(help_text="Rating")
    content = models.TextField(help_text="Content")

    def __str__(self):
        """String representation of the model"""
        return "{recipe}'s review by {author}".format(
            recipe=self.recipe.name,
            author=self.author.username,
        )

class Day(CibiTrackedModel):
    """
    Recipe day model
    Day concept
    """
    date = models.DateField(help_text="Day")
    users = models.ManyToManyField("auth.User", blank=True)

    @classmethod
    def date_from_string(self, string_date=None):
        if string_date is None:
            return datetime.date.today()
        else:
            return datetime.datetime.strptime(string_date, "%d/%m/%Y").date()

    @property
    def meals(self):
        return self.meal_set.all()

    def __str__(self):
        return self.date.strftime("%A %d %B %Y")



class Meal(CibiTrackedModel):
    """
    Recipe meal model
    Meal concept
    """
    name = models.CharField(default="", max_length=128, help_text="Name of the meal")
    persons = models.FloatField(default=1.0, help_text="Number of persons")
    day = models.ForeignKey("cibi.Day", on_delete=models.CASCADE)
    recipes = models.ManyToManyField("cibi.Recipe", blank=True)

    def __str__(self):
        recipes = ", ".join(map(str, self.recipes.all()))
        menu = recipes or "No recipes specified."
        return "{date} ({name}) - {menu}".format(
            date=self.day,
            name=self.name,
            menu=menu,
        )
