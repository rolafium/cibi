"""
Tests for the cibi serializers
module: cibi.serializers
"""
from rest_framework.serializers import ModelSerializer
from cibi.serializers import (
    CibiModelSerializer,
    UserSerializer,
    CibiTagSerializer,
    IngredientSerializer,
    RecipeSerializer,
    RecipeStepSerializer,
    RecipeFeedbackSerializer,
)


def check_type_and_fields(checked_class, field_list):
    """Checks if the class is correctly defined"""
    assert issubclass(checked_class, CibiModelSerializer)
    assert all(field in checked_class.Meta.fields for field in field_list)

def test_cibi_model_serializer_type():
    """Checks if CibiModelSerializer is subclass of ModelSerializer"""
    assert issubclass(CibiModelSerializer, ModelSerializer)

def test_user_serializer_type():
    """Checks the UserSerializer type"""
    check_type_and_fields(UserSerializer, ["id", "email", "password"])

def test_cibi_tag_serializer_type():
    """Checks the CibiTagSerializer type"""
    check_type_and_fields(CibiTagSerializer, ["name"])

def test_cibi_ingredient_serializer_type():
    """Checks the IngredientSerializer type"""
    check_type_and_fields(IngredientSerializer, ["name", "description", "tags"])

def test_recipe_serializer_type():
    """Checks the RecipeSerializer type"""
    check_type_and_fields(RecipeSerializer, ["name", "description", "tags", "number_of_persons"])

def test_recipe_step_serializer_type():
    """Checks the RecipeStepSerializer type"""
    check_type_and_fields(RecipeStepSerializer, ["recipe", "content", "optional"])

def test_recipe_feedback_serializer_type():
    """Checks the RecipeFeedbackSerializer type"""
    check_type_and_fields(RecipeFeedbackSerializer, ["recipe", "author", "rating", "content"])
