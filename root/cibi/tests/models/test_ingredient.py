"""
Tests for Ingredient
module: cibi.models
"""
from cibi.models import Ingredient


def test_str_method():
    """Tests the str method"""
    ing = Ingredient(name="Chilli")
    assert str(ing) == "Chilli"
