"""
Tests for RecipeFeedback
module: cibi.models
"""
import pytest
from cibi.models import Recipe, RecipeFeedback


pytestmark = pytest.mark.django_db(transaction=True) # pylint: disable=C0103

def test_str_method(admin_user):
    """Tests the str method"""
    recipe = Recipe.objects.create(name="Lasagna")
    feedback = RecipeFeedback.objects.create(
        recipe=recipe,
        author=admin_user,
        rating=10,
        content="Wow!",
    )

    assert str(feedback) == "Lasagna's review by {user}".format(user=admin_user)
