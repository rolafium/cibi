"""
Tests for CibiTag
module: cibi.models
"""
from cibi.models import CibiTag


def test_cibitag_str():
    """Tests the str method"""
    tag = CibiTag(name="Hello")
    assert str(tag) == "Hello"
