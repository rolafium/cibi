"""
Tests for CibiTrackedModel
module: cibi.models
"""
import pytest
from cibi.models import Ingredient


pytestmark = pytest.mark.django_db(transaction=True) # pylint: disable=c0103

def test_save():
    """Created at should be smaller then Updated at """
    ing = Ingredient()
    ing.save()
    ing.save()
    assert ing.updated_at > ing.created_at
