"""
Tests for Recipe
module: cibi.models
"""
from cibi.models import Recipe


def test_str_method():
    """Tests the str method"""
    recipe = Recipe(name="Lasagna")
    assert str(recipe) == "Lasagna"
