"""
Tests for RecipeStep
module: cibi.models
"""
import pytest
from cibi.models import Recipe, RecipeStep


pytestmark = pytest.mark.django_db(transaction=True) # pylint: disable=C0103

def test_str_method():
    """Tests the str method"""
    recipe = Recipe.objects.create(name="Lasagna")
    recipe_step = RecipeStep.objects.create(
        recipe=recipe,
        content="Last step: Eat it!",
    )

    assert str(recipe_step) == "Lasagna's step"
