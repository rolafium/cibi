"""
Test utils
"""
import random
from unittest.mock import MagicMock
from django.test import RequestFactory


REQUEST_FACTORY = RequestFactory()

def create_mock(defaults=None, overrides=None, mock_name="__MOCK__"):
    """
    Creates a mocked object as defined
    :param defaults: dict/None with the default values
    :param overrides: dict/None with the overrides
    :returns: MagicMock
    """
    if defaults is None:
        defaults = {}
    if overrides is None:
        overrides = {}

    obj = MagicMock()
    obj.__mockname__ = mock_name
    obj.__defaults__ = defaults
    obj.__overrides__ = overrides

    properties = { **defaults, **overrides }

    for key in properties:
        setattr(obj, key, properties[key])

    return obj


def create_mock_user(user_id=901, **overrides):
    """
    Creates a mocked django user
    :param id: int
    :param username: str
    :param email: str
    :param password: str
    :param is_staff: bool
    :param is_superuser: bool
    :returns: MagicMock
    """
    user_defaults = {
        "id": 901,
        "username": "__USER__",
        "email": "__EMAIL__",
        "password": "__PASSWORD__",
        "is_authenticated": True,
        "is_staff": False,
        "is_superuser": False,
    }
    overrides["id"] = user_id

    user = create_mock(
        defaults=user_defaults,
        overrides=overrides,
        mock_name="__USER__"
    )
    return user

def create_request(method="get", url="/", user=None, data=None):
    """
    Creates a WSGI Request
    request.user is populated with either the provided user
    or a mocked user generated with create_mock_user
    :param method: str
    :param url: str
    :param data: dict
    :returns: WSGI Request
    """
    if user is None:
        user = create_mock_user()
    if data is None:
        data = {}

    request = getattr(REQUEST_FACTORY, method.lower())(url)
    request.user = user
    request.data = data
    return request


def create_viewset(viewset, request=None, viewset_kwargs=None):
    """
    Creates a ViewSet
    viewset.request is populated with either the provided request
    or a WSGI request generated with create_request
    :param viewset: ModelViewSet
    :param request: WSGI Request / MagicMock
    :param viewset_kwargs: dict
    :returns: ModelViewSet
    """
    if request is None:
        request = create_request()
    if viewset_kwargs is None:
        viewset_kwargs = {}

    viewset = viewset()
    viewset.request = request
    viewset.kwargs = viewset_kwargs
    viewset.format_kwarg = viewset_kwargs
    return viewset

def create_serializer_class(is_valid=False, data=None, save=None, save_returns=None):
    """
    Creates a mocked serializer class
    :param is_valid: bool
    :param data: dict
    :param save: function
    :param save_returns:
    :returns: MagicMock
    """
    if data is None:
        rand_key = "rand-key-" + str(random.randint(0, 10**6))
        rand_val = "rand-val" + str(random.randint(0, 10**6))
        data = { "data": "data", rand_key: rand_val }
    if save is None:
        save = MagicMock()
        if save_returns is None:
            save_returns = data
        save.return_value = save_returns

    serializer = MagicMock()
    serializer.data = data
    serializer.validated_data = data
    serializer.is_valid.return_value = is_valid
    serializer.save.return_value = save
    serializer_class = MagicMock()
    serializer_class.return_value = serializer
    serializer_class.__serializer__ = serializer
    serializer_class.__data__ = data
    serializer_class.__save__ = save
    serializer_class.__savereturns__ = save_returns


    return serializer_class
