"""
Tests for the cibi exceptions
module: cibi.exceptions
"""
from rest_framework.exceptions import APIException
from cibi.exceptions import (
    CibiException,
    PermissionDenied,
    InvalidRequest,
)


def test_cibi_exception_type():
    """Tests the type of CibiException"""
    assert issubclass(CibiException, APIException)


def test_permission_denied_type():
    """Tests the type of PermissionDenied"""
    assert issubclass(PermissionDenied, CibiException)
    assert PermissionDenied.status_code == 403

def test_invalid_request_type():
    """Tests the type of InvalidRequest"""
    assert issubclass(InvalidRequest, CibiException)
    assert InvalidRequest.status_code == 400
