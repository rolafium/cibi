"""
Tests the generic views
module: cibi.views.generic
"""
from unittest.mock import patch
from cibi.views.generic import cibi_landing


@patch("cibi.views.generic.render")
def test_cibi_landing(render_mock):
    """Checks the render function is called with the correct params"""
    request = "__REQUEST__"
    context = {}

    cibi_landing(request)
    assert render_mock.call_count == 1
    assert render_mock.call_args[0][0] == request
    assert render_mock.call_args[0][1] == "cibi/landing.html"
    assert render_mock.call_args[0][2] == context
