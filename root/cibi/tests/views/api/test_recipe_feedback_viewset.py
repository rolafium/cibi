"""
Tests for the RecipeFeedback viewsets
module: cibi.views.api.recipe_feedback_viewset
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.views.api.recipe_feedback_viewset import RecipeFeedbackViewSet


def test_recipe_feedback_viewset_type():
    """Checks the RecipeFeedbackViewSet type"""
    assert issubclass(RecipeFeedbackViewSet, CibiModelViewSet)
    assert hasattr(RecipeFeedbackViewSet, "serializer_class")
    assert hasattr(RecipeFeedbackViewSet, "queryset")
