"""
Tests for the Ingredient viewsets
module: cibi.views.api.ingredient_viewset
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.views.api.ingredient_viewset import IngredientViewSet


def test_ingredient_viewset_type():
    """Checks the IngredientViewSet type"""
    assert issubclass(IngredientViewSet, CibiModelViewSet)
    assert hasattr(IngredientViewSet, "serializer_class")
    assert hasattr(IngredientViewSet, "queryset")
