"""
Tests for the Recipe viewsets
module: cibi.views.api.recipe_viewset
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.views.api.recipe_viewset import RecipeViewSet


def test_recipe_viewset_type():
    """Checks the RecipeViewSet type"""
    assert issubclass(RecipeViewSet, CibiModelViewSet)
    assert hasattr(RecipeViewSet, "serializer_class")
    assert hasattr(RecipeViewSet, "queryset")
