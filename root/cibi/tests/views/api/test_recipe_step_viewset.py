"""
Tests for the RecipeStep viewsets
module: cibi.views.api.recipe_step_viewset
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.views.api.recipe_step_viewset import RecipeStepViewSet


def test_recipe_step_viewset_type():
    """Checks the RecipeStepViewSet type"""
    assert issubclass(RecipeStepViewSet, CibiModelViewSet)
    assert hasattr(RecipeStepViewSet, "serializer_class")
    assert hasattr(RecipeStepViewSet, "queryset")
