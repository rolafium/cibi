"""
Tests for the CibiTag viewsets
module: cibi.views.api.cibi_tag_viewset
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.views.api.cibi_tag_viewset import CibiTagViewSet


def test_cibi_tag_viewset_type():
    """Checks the CibiTagViewSet type"""
    assert issubclass(CibiTagViewSet, CibiModelViewSet)
    assert hasattr(CibiTagViewSet, "serializer_class")
    assert hasattr(CibiTagViewSet, "queryset")
