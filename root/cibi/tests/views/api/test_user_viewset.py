"""
Tests for the User viewsets
module: cibi.views.api.user_viewset
"""
from unittest.mock import patch

import pytest
from rest_framework import status
from cibi.tests.testing_utils.mocks import create_mock_user, create_request, create_viewset, create_serializer_class
from cibi.exceptions import InvalidRequest, PermissionDenied
from cibi.views.api.abstract import CibiModelViewSet
from cibi.views.api.user_viewset import UserViewSet


def test_user_viewset_type():
    """Checks the UserViewSet type"""
    assert issubclass(UserViewSet, CibiModelViewSet)
    assert hasattr(UserViewSet, "serializer_class")
    assert hasattr(UserViewSet, "queryset")


def test_create_no_data_fails():
    """Create fails with no data"""
    request_data = {}
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request)
    with pytest.raises(InvalidRequest):
        viewset.create(request)

def test_create_no_email_fails():
    """Create fails with no email provided"""
    request_data = { "password": "__PASSWORD__" }
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request)
    with pytest.raises(InvalidRequest):
        viewset.create(request)

def test_create_no_password_fails():
    """Create fails with no password provided"""
    request_data = { "email": "__EMAIL__" }
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request)
    with pytest.raises(InvalidRequest):
        viewset.create(request)

@patch("cibi.views.api.user_viewset.login")
@patch("cibi.views.api.user_viewset.User")
def test_create_success(mock_user, mock_login):
    """Create is successful with the proper data"""
    request_data = { "email": "__EMAIL__", "password": "__PASSWORD__" }
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request)
    viewset.serializer_class = create_serializer_class()

    response = viewset.create(viewset.request)
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data == viewset.serializer_class.__data__

    mock_create_user = mock_user.objects.create_user
    assert mock_create_user.call_count == 1
    assert mock_create_user.call_args[0][0] == request.data["email"]
    assert mock_create_user.call_args[0][1] == request.data["email"]
    assert mock_create_user.call_args[0][2] == request.data["password"]

    assert mock_login.call_count == 1
    assert mock_login.call_args[0][0] == request
    assert mock_login.call_args[0][1] == mock_create_user()

    assert viewset.serializer_class.call_count == 1
    assert viewset.serializer_class.call_args[0][0] == mock_create_user()

@patch("cibi.views.api.user_viewset.UserViewSet.get_object")
def test_user_tries_update_another_user(get_object_mock):
    """Users can't update other users"""
    request_data = {}
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request, viewset_kwargs={ "pk": 4041 })
    get_object_mock.return_value = create_mock_user(user_id=4041)
    with pytest.raises(PermissionDenied):
        viewset.update(request, pk=4041)

@patch("cibi.views.api.user_viewset.UserViewSet.get_object")
def test_user_updates_themselves(get_object_mock):
    """Users can update themselves"""
    get_object_mock.return_value = create_mock_user(user_id=4041)
    request_data = {}
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request, viewset_kwargs={ "pk": request.user.id })
    viewset.serializer_class = create_serializer_class(is_valid=True)
    get_object_mock.return_value = create_mock_user(user_id=request.user.id)

    response = viewset.update(request, pk=request.user.id)
    assert response.status_code == status.HTTP_200_OK
    assert response.data == viewset.serializer_class.__data__

    serializer = viewset.serializer_class.__serializer__
    assert serializer.is_valid.call_count == 1
    assert serializer.save.call_count == 1

@patch("cibi.views.api.user_viewset.UserViewSet.get_object")
def test_superuser_updates_another_user(get_object_mock):
    """Superusers can update other users"""
    get_object_mock.return_value = create_mock_user(user_id=999)
    request_data = {}
    request = create_request(data=request_data)
    request.user.is_superuser = True
    viewset = create_viewset(UserViewSet, request=request, viewset_kwargs={ "pk": 4041 })
    viewset.serializer_class = create_serializer_class(is_valid=True)
    get_object_mock.return_value = create_mock_user(user_id=4041)

    response = viewset.update(request, pk=request.user.id)
    assert response.status_code == status.HTTP_200_OK
    assert response.data == viewset.serializer_class.__data__

    serializer = viewset.serializer_class.__serializer__
    assert serializer.is_valid.call_count == 1
    assert serializer.save.call_count == 1

@patch("cibi.views.api.user_viewset.UserViewSet.get_object")
def test_update_invalid_data(get_object_mock):
    """Update fails with invalid data"""
    get_object_mock.return_value = create_mock_user(user_id=999)
    request_data = {}
    request = create_request(data=request_data)
    request.user.is_superuser = True
    viewset = create_viewset(UserViewSet, request=request, viewset_kwargs={ "pk": 4041 })
    viewset.serializer_class = create_serializer_class(is_valid=False)
    get_object_mock.return_value = create_mock_user(user_id=4041)

    with pytest.raises(InvalidRequest):
        viewset.update(request, pk=request.user.id)

def test_login_no_data_fails():
    """Login fails with no data"""
    request_data = {}
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request)
    with pytest.raises(InvalidRequest):
        viewset.login(request)

def test_login_no_email_fails():
    """Login fails with no email"""
    request_data = { "password": "__PASSWORD__" }
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request)
    with pytest.raises(InvalidRequest):
        viewset.login(request)

def test_login_no_password_fails():
    """Login fails with no password"""
    request_data = { "email": "__EMAIL__" }
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request)
    with pytest.raises(InvalidRequest):
        viewset.login(request)

@patch("cibi.views.api.user_viewset.authenticate")
def test_login_bad_credentias(auth_mock):
    """Login fails with bad credentials"""
    auth_mock.return_value = None
    request_data = { "email": "__EMAIL__", "password": "__PASSWORD__" }
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request)
    with pytest.raises(PermissionDenied):
        viewset.login(request)

@patch("cibi.views.api.user_viewset.login")
@patch("cibi.views.api.user_viewset.authenticate")
def test_login_good_credentials(auth_mock, login_mock):
    """Login is successful with good credentials"""
    user = create_mock_user(user_id=405)
    auth_mock.return_value = user
    request_data = { "email": "__EMAIL__", "password": "__PASSWORD__" }
    request = create_request(data=request_data)
    viewset = create_viewset(UserViewSet, request=request)
    viewset.serializer_class = create_serializer_class()

    response = viewset.login(request)
    assert response.status_code == status.HTTP_200_OK
    assert response.data == viewset.serializer_class.__data__
    assert login_mock.call_count == 1
    assert login_mock.call_args[0][0] == request
    assert login_mock.call_args[0][1] == user

def test_logout_anon_user():
    """Logged out users can't logout"""
    request = create_request()
    request.user.is_authenticated = False
    viewset = create_viewset(UserViewSet, request=request)
    with pytest.raises(InvalidRequest):
        viewset.logout(request)

@patch("cibi.views.api.user_viewset.logout")
def test_logout_auth_user(logout_mock):
    """Logged in users correctly logout"""
    request = create_request()
    viewset = create_viewset(UserViewSet, request=request)
    viewset.serializer_class = create_serializer_class()

    response = viewset.logout(request)
    assert response.status_code == status.HTTP_200_OK
    assert response.data == viewset.serializer_class.__data__
    assert logout_mock.call_count == 1
    assert logout_mock.call_args[0][0] == request
