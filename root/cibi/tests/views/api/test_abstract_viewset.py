"""
Tests for the abstract viewsets
module: cibi.views.api.abstract
"""
from rest_framework.viewsets import ModelViewSet
from cibi.views.api.abstract import CibiModelViewSet


def test_cibi_model_viewset_type():
    """Checks CibiModelViewSet is subclass of ModelViewSet"""
    assert issubclass(CibiModelViewSet, ModelViewSet)
