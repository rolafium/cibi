"""
Generic views
"""
from django.shortcuts import render


def cibi_landing(request):
    """
    Landing page
    :param request: WSGI Request
    """
    template = "cibi/landing.html"
    context = {}
    return render(request, template, context)
