"""
Viewsets for CibiTag
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.models import CibiTag
from cibi.serializers import CibiTagSerializer


class CibiTagViewSet(CibiModelViewSet):
    """CibiTag viewset"""
    serializer_class = CibiTagSerializer
    queryset = CibiTag.objects.all()
