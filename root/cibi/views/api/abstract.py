"""
Abstract Viewsets
"""
from rest_framework.viewsets import ModelViewSet


class CibiModelViewSet(ModelViewSet):
    """Abstract cibi model view set"""
    pass
