"""
Viewsets for RecipeIngredient
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.models import RecipeIngredient
from cibi.serializers import RecipeIngredientSerializer


class RecipeIngredientViewSet(CibiModelViewSet):
    """RecipeIngredient viewset"""
    serializer_class = RecipeIngredientSerializer
    queryset = RecipeIngredient.objects.all()



def create_recipe_ingredient(request, data=None):
    if data is None:
        data = request.data

    serializer = RecipeIngredientSerializer(data=data)

    recipe_ingredient = None

    if serializer.is_valid(raise_exception=True):
        recipe_ingredient = serializer.save()

    return serializer.data.copy()
