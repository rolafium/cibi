"""
Viewsets for RecipeFeedback
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.models import RecipeFeedback
from cibi.serializers import RecipeFeedbackSerializer


class RecipeFeedbackViewSet(CibiModelViewSet):
    """RecipeFeedback viewset"""
    serializer_class = RecipeFeedbackSerializer
    queryset = RecipeFeedback.objects.all()
