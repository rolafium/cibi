"""
Viewsets for Meal
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.models import Meal
from cibi.serializers import MealSerializer, MealDetailSerializer


class MealViewSet(CibiModelViewSet):
    """Meal viewset"""
    serializer_class = MealSerializer
    queryset = Meal.objects.all()

    def get_serializer_class(self):
        if self.action == "retrieve":
            return MealDetailSerializer

        return self.serializer_class
