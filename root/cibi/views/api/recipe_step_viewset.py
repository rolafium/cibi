"""
Viewsets for RecipeStep
"""
from cibi.views.api.abstract import CibiModelViewSet
from cibi.models import RecipeStep
from cibi.serializers import RecipeStepSerializer


class RecipeStepViewSet(CibiModelViewSet):
    """RecipeStep viewset"""
    serializer_class = RecipeStepSerializer
    queryset = RecipeStep.objects.all()
