"""
Viewsets for Day
"""
import datetime
from django.db.models import Q
from rest_framework.decorators import action
from rest_framework.response import Response
from cibi.views.api.abstract import CibiModelViewSet
from cibi.models import Day, Meal
from cibi.serializers import DaySerializer, DayDetailSerializer


class DayViewSet(CibiModelViewSet):
    """Day viewset"""
    serializer_class = DaySerializer
    queryset = Day.objects.all()

    def get_queryset(self):
        return self.queryset.filter(users=self.request.user)

    def get_serializer_class(self):
        if self.action == "retrieve":
            return DayDetailSerializer

        return self.serializer_class

    @action(methods=["GET"], detail=False, url_name="create-week", url_path="create-week")
    def create_week(self, request, *args, **kwargs):
        starting_day = Day.date_from_string(request.query_params.get("starting", None))
        meal_names = request.query_params.get("meal_names", "").split(",")
        persons = int(request.query_params.get("persons", 1))

        week = []

        for i in range(7):
            new_date = starting_day + datetime.timedelta(days=i)
            day = Day.objects.create(date=new_date)
            day.users.add(request.user)

            for meal_name in meal_names:
                Meal.objects.create(
                    name=meal_name,
                    persons=persons,
                    day=day,
                )

            week.append(day)

        serializer_class = self.get_serializer_class()
        serializer = serializer_class(week, many=True)

        return Response(serializer.data)


    @action(methods=["GET"], detail=False, url_name="grocery-list", url_path="grocery-list")
    def grocery_list(self, request, *args, **kwargs):
        starting_day = Day.date_from_string(request.query_params.get("starting", None))
        date_range = Q(date__gte=starting_day)

        ending_day_str = request.query_params.get("ending", None)
        if ending_day_str:
            ending_day = Day.date_from_string(ending_day_str)
            date_range &= Q(date__lte=ending_day)

        days = self.queryset.filter(date_range)
        ingredients = {}
        recipes = {}

        for day in days:
            for meal in day.meals:
                for recipe in meal.recipes.all():
                    if not recipe.id in recipes:
                        recipes[recipe.id] = {
                            "name": recipe.name,
                            "quantity": 0
                        }
                    recipes[recipe.id]["quantity"] += recipe.number_of_persons

                    for recipe_ingredient in recipe.ingredients:
                        ingredient = recipe_ingredient.ingredient
                        quantity = recipe_ingredient.quantity
                        if not ingredient.id in ingredients:
                            ingredients[ingredient.id] = {
                                "name": ingredient.name,
                                "quantity": 0,
                            }
                        ingredients[ingredient.id]["quantity"] += quantity * meal.persons / recipe.number_of_persons

        return Response({
            "ingredients": ingredients.values(),
            "recipes": recipes.values(),
        })
