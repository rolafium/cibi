"""
Viewsets for Recipe
"""
from django.db import transaction
from rest_framework.response import Response
from cibi.views.api.abstract import CibiModelViewSet
from cibi.views.api.recipe_ingredient_viewset import create_recipe_ingredient
from cibi.models import Recipe
from cibi.serializers import RecipeSerializer, RecipeDetailSerializer


class RecipeViewSet(CibiModelViewSet):
    """Recipe viewset"""
    serializer_class = RecipeSerializer
    queryset = Recipe.objects.all()

    def get_serializer_class(self):
        if self.action == "retrieve":
            return RecipeDetailSerializer

        return self.serializer_class

    def create(self, request, *args, **kwargs):
        response_data = create_recipe(request, data=request.data)
        return Response(response_data)



def create_recipe(request, data=None):
    if data is None:
        data = request.data

    serializer = RecipeSerializer(data=data)

    recipe = None
    if serializer.is_valid(raise_exception=True):
        recipe = serializer.save()

    ri_datas = data.get("recipe_ingredients", [])
    recipe_ingredients = []

    for ri_data in ri_datas:
        server_ri_data = {
            "recipe": recipe.id,
            "ingredient": ri_data["ingredient"]["id"],
            "quantity": ri_data["quantity"]
        }
        recipe_ingredient = create_recipe_ingredient(request, data=server_ri_data)
        recipe_ingredients.append(recipe_ingredient)

    response_data = serializer.data.copy()
    response_data["recipe_ingredients"] = recipe_ingredients

    return response_data
