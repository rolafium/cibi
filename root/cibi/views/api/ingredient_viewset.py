"""
Viewsets for Ingredient
"""
from rest_framework.decorators import action
from rest_framework.response import Response
from cibi.exceptions import InvalidRequest
from cibi.views.api.abstract import CibiModelViewSet
from cibi.models import Ingredient
from cibi.serializers import IngredientSerializer


class IngredientViewSet(CibiModelViewSet):
    """Ingredient viewset"""
    serializer_class = IngredientSerializer
    queryset = Ingredient.objects.all()

    def get_queryset(self):
        search = self.request.query_params.get("search", None)
        queryset = self.queryset

        if search is not None:
            queryset = queryset.filter(name__icontains=search)

        return queryset

    @action(methods=["GET"], detail=False, url_path="by-recipe", url_name="by-recipe")
    def by_recipe(self, request, *args, **kwargs):
        recipe_id = request.query_params.get("recipe", None)

        if recipe_id is None:
            raise InvalidRequest("`recipe` is required field.")

        recipe_id = int(recipe_id)

        ingredients = Ingredient.objects.filter(recipe=recipe_id)
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(ingredients, many=True)

        return Response(serializer.data)
