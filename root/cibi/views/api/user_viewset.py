"""
Viewsets for User
"""
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from rest_framework import status
from rest_framework.decorators import action, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from cibi.exceptions import InvalidRequest, PermissionDenied
from cibi.views.api.abstract import CibiModelViewSet
from cibi.serializers import UserSerializer


class UserViewSet(CibiModelViewSet):
    """User viewset"""
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_permissions(self):
        if self.action == "login":
            return [AllowAny()]
        else:
            return [IsAuthenticated()]

    def create(self, request, *args, **kwargs):
        """Registers and logins a new user"""
        try:
            username = request.data["email"]
            email = request.data["email"]
            password = request.data["password"]
            user = User.objects.create_user(username, email, password)
            login(request, user)
            serializer = self.serializer_class(user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except (KeyError, IntegrityError):
            raise InvalidRequest

    def update(self, request, *args, **kwargs):
        """Updates a user"""
        user = self.get_object()

        if not request.user.is_superuser and not user.id == request.user.id:
            raise PermissionDenied

        serializer = self.serializer_class(user, data=request.data)

        if not serializer.is_valid():
            raise InvalidRequest

        serializer.save()
        return Response(serializer.data)

    @action(methods=["POST"], detail=False, url_path="login", url_name="login")
    def login(self, request, *args, **kwargs):
        """Logins a user"""
        user = None

        print("HELLO", request.data)
        try:
            username = request.data["email"]
            password = request.data["password"]
            user = authenticate(request, username=username, password=password)
        except KeyError:
            raise InvalidRequest

        if user is None:
            raise PermissionDenied

        login(request, user)

        serializer = self.serializer_class(user)
        return Response(serializer.data)

    @action(methods=["POST"], detail=False, url_path="logout", url_name="logout")
    def logout(self, request, *args, **kwargs):
        """Logouts a user"""
        if not request.user.is_authenticated:
            raise InvalidRequest

        logout(request)

        serializer = self.serializer_class(request.user)
        return Response(serializer.data)
