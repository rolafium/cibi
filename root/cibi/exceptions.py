"""
Exceptions for the cibi app
"""
from rest_framework.exceptions import APIException


class CibiException(APIException):
    """Cibi abstract exception"""
    pass

class PermissionDenied(CibiException):
    """Permission denied"""
    status_code = 403
    default_code = "http_403_permission_denied"
    default_detail = "Permission Denied"

class InvalidRequest(CibiException):
    """Invalid request"""
    status_code = 400
    default_code = "http_400_invalid_request"
    default_detail = "Invalid Request"
