"""
Apps module for cibi
"""
from django.apps import AppConfig


class CibiConfig(AppConfig):
    """CibiConfig class"""
    name = 'cibi'
