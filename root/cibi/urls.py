"""
Urls for cibi
"""
from django.urls import include, path, re_path
from rest_framework import routers
from cibi.views.generic import cibi_landing
from cibi.views.api.cibi_tag_viewset import CibiTagViewSet
from cibi.views.api.ingredient_viewset import IngredientViewSet
from cibi.views.api.recipe_viewset import RecipeViewSet
from cibi.views.api.recipe_ingredient_viewset import RecipeIngredientViewSet
from cibi.views.api.recipe_step_viewset import RecipeStepViewSet
from cibi.views.api.recipe_feedback_viewset import RecipeFeedbackViewSet
from cibi.views.api.user_viewset import UserViewSet
from cibi.views.api.day_viewset import DayViewSet
from cibi.views.api.meal_viewset import MealViewSet


router = routers.DefaultRouter() # pylint: disable=C0103
router.register(r"tags", CibiTagViewSet)
router.register(r"ingredients", IngredientViewSet)
router.register(r"recipes", RecipeViewSet)
router.register(r"recipes-ingredients", RecipeIngredientViewSet)
router.register(r"recipes-steps", RecipeStepViewSet)
router.register(r"recipes-feedbacks", RecipeFeedbackViewSet)
router.register(r"users", UserViewSet)
router.register(r"days", DayViewSet)
router.register(r"meals", MealViewSet)


urlpatterns = [
    path("api/", include(router.urls)),
    path("home", cibi_landing, name="cibi_landing_home"),
    re_path("", cibi_landing, name="cibi_landing_absolute"),
]
