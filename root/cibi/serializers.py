"""
Serializers for the cibi models
"""
from django.contrib.auth.models import User
from rest_framework import serializers
from cibi import models


class CibiModelSerializer(serializers.ModelSerializer):
    """Abstract cibi model serializer"""
    pass

class UserSerializer(CibiModelSerializer):
    """User Serializer"""
    password = serializers.CharField(write_only=True, style={ "input_type": "password" })

    class Meta:
        """Meta class"""
        model = User
        fields = (
            "id",
            "email",
            "password",
        )

class CibiTagSerializer(CibiModelSerializer):
    """CibiTag serializer"""
    class Meta:
        """Meta class"""
        model = models.CibiTag
        fields = (
            "id",
            "name",
        )

class IngredientSerializer(CibiModelSerializer):
    """Ingredient serializer"""

    class Meta:
        """Meta class"""
        model = models.Ingredient
        fields = (
            "id",
            "name",
            "description",
            "tags",
        )

class RecipeSerializer(CibiModelSerializer):
    """Recipe serializer"""
    class Meta:
        """Meta class"""
        model = models.Recipe
        fields = (
            "id",
            "name",
            "description",
            "tags",
            "number_of_persons",
        )

class RecipeIngredientSerializer(CibiModelSerializer):
    """RecipeIngredient serializer"""
    class Meta:
        """Meta class"""
        model = models.RecipeIngredient
        fields = (
            "id",
            "recipe",
            "ingredient",
            "quantity",
        )

class RecipeIngredientDetailSerializer(RecipeIngredientSerializer):
    ingredient = IngredientSerializer()

class RecipeStepSerializer(CibiModelSerializer):
    """RecipeStep serializer"""
    class Meta:
        """Meta class"""
        model = models.RecipeStep
        fields = (
            "id",
            "recipe",
            "content",
            "optional",
        )

class RecipeDetailSerializer(RecipeSerializer):
    """Recipe detail serializer"""
    ingredients = RecipeIngredientDetailSerializer(many=True)
    steps = RecipeStepSerializer(many=True)

    class Meta:
        """Meta class"""
        model = models.Recipe
        fields = RecipeSerializer.Meta.fields + (
            "ingredients",
            "steps",
        )



class RecipeFeedbackSerializer(CibiModelSerializer):
    """RecipeFeedback serializer"""
    class Meta:
        """Meta class"""
        model = models.RecipeFeedback
        fields = (
            "id",
            "recipe",
            "author",
            "rating",
            "content",
        )



class DaySerializer(CibiModelSerializer):
    """Day serializer"""
    class Meta:
        """Meta class"""
        model = models.Day
        fields = (
            "id",
            "date",
        )

class MealSerializer(CibiModelSerializer):
    """Day serializer"""

    class Meta:
        """Meta class"""
        model = models.Meal
        fields = (
            "id",
            "name",
            "persons",
            "day",
            "recipe",
        )


class MealDetailSerializer(MealSerializer):
    day = DaySerializer()
    recipe = RecipeDetailSerializer()

class DayDetailSerializer(DaySerializer):
    meals = MealDetailSerializer(source="meal_set", many=True)
    class Meta:
        model = models.Day
        fields = DaySerializer.Meta.fields + (
            "meals",
        )
