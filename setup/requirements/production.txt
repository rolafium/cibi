Django==2.0.7
djangorestframework==3.8.2
psycopg2==2.7.5
psycopg2-binary==2.7.5
git+https://gitlab.com/rolafium/pydenv.git
git+https://gitlab.com/rolafium/django-request-cop.git
git+https://gitlab.com/rolafium/django-classy-settings.git
